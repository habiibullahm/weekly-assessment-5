const filterArray = (data) =>{
    let res = []
    data.forEach(element => {
        let item = Number(element)
        if (item >= 0 && !res.includes(item)) res.push(item)
    })
    return res
}

console.log(filterArray([1, 2, "a", "b"]));
console.log(filterArray([1, "a", "b", 0, 15]));
console.log(filterArray([1, 2, "aasf", "1", "123",1 , 123]));


const dropRight = (arr, n=1) => { 

    return arr.slice(0, -n)
}

console.log(dropRight([1, 2, 3]))
console.log(dropRight([1, 2, 3], 2))
console.log(dropRight([1, 2, 3], 5))


const sumOfCubes=(data)=> {
    let total = 0
    data.forEach(item => {
        total += Math.pow(item, 3)
        
        
    });
    return total
 }
  
console.log(sumOfCubes([1,5,9]))
console.log(sumOfCubes([3, 4, 5]))
console.log(sumOfCubes([2]))
console.log(sumOfCubes([]))

